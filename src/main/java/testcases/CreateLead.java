package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import utils.ProjectSpecificMethod;

public class CreateLead extends ProjectSpecificMethod {

	@BeforeTest
	public void setData() {
		excelfilename="CreateLead";
	}

	@Test(dataProvider="fetchData", invocationCount=2)
	public void cLead(String companyName, String firstName, String lastName, String phoneNumber) {
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys(companyName);
		driver.findElementById("createLeadForm_firstName").sendKeys(firstName);
		driver.findElementById("createLeadForm_lastName").sendKeys(lastName);
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys(phoneNumber);
		driver.findElementByName("submitButton").click();
	}
}






